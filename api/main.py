from typing import Union

from fastapi import FastAPI
import psycopg
# from pydantic import BaseModel


app = FastAPI()



# class Item(BaseModel):
#     name: str
#     price: float
#     is_offer: Union[bool, None] = None



@app.get("/hello-world")
async def root():
    return {"message": "Hello World"}


@app.get("/api/categories")
def list_categories():
    conn_str="postgresql://trivia-game:trivia-game@db/trivia-game"
    with psycopg.connect(conn_str) as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT id, title, canon
                FROM categories
                LIMIT 100
            """)
            records = cur.fetchall()
            results = []
            for record in records:
                result = {
                    "id": record[0],
                    "title": record[1],
                    "canon": record[2],
                }
                results.append(result)
            return results


# @app.get("/api/categories")
# def list_categories():
#     return []

# @app.get("/items/{item_id}")
# def read_item(item_id:int, q: Union[str, None] = None):
#     return {"item_id" : item_id, "q":q}

# @app.put("/items/{item_id}")
# def update_item(item_id: int, item: Item):
#     return {"item_name": item.name, "item_id": item_id}